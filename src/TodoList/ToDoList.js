import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ThemeProvider } from 'styled-components'
import { addTaskAction, changeThemeAction, deletaTaskAction, doneTaskAction, editTaskAction, updateTaskAction } from '../redux/actions/ToDoListActions'
import { change_theme } from '../redux/types/ToDoListTypes'
import { Button } from './components/Button'
import { Container } from './components/Container'
import { Dropdown } from './components/Dropdown'
import { Heading1, Heading2, Heading3 } from './components/Heading'
import { Table, Th, Thead, Tr } from './components/Table'
import { TextField } from './components/TextField'
import { arrTheme } from './themes/ThemeManager'
import { ToDoListDarkTheme } from './themes/ToDoListDarkTheme'
import { ToDoListLightTheme } from './themes/ToDoListLightTheme'
import { ToDoListPrimaryTheme } from './themes/ToDoListPrimaryTheme'

class ToDoList extends Component {
    state ={
        taskName: '',
        disabled: true,
    }

    renderTaskToDo = () => {
        return this.props.taskList.filter(task => !task.done).map(
            (task, index) => {
                return (
                    <Tr key={index}>
                        <Th style={{verticalAlign: 'middle'}}>{task.taskName}</Th>
                        <Th className='text-right'>
                            <Button 
                            onClick={() => {
                                this.setState({
                                    disabled: false
                                }, () => {
                                    this.props.dispatch(editTaskAction(task))
                                })
                            }}
                            ><i className="fa-solid fa-pen-to-square"></i></Button>
                            <Button 
                            onClick={() => {this.props.dispatch(doneTaskAction(task.id))}}
                            className='mx-1'><i className="fa-solid fa-check"></i></Button>
                            <Button 
                            onClick={() => {this.props.dispatch(deletaTaskAction(task.id))}}
                            ><i className="fa-solid fa-trash"></i></Button>
                        </Th>
                    </Tr>
                )
            }
        )
    }
    
    renderTaskCompleted = () => {
        return this.props.taskList.filter(task => task.done).map(
            (task, index) => {
                return (
                    <Tr key={index}>
                        <Th style={{verticalAlign: 'middle'}}>{task.taskName}</Th>
                        <Th className='text-right'>
                            <Button
                            onClick={() => {this.props.dispatch(deletaTaskAction(task.id))}}
                            ><i className="fa-solid fa-trash"></i></Button>
                        </Th>
                    </Tr>
                )
            }
        )
    }
    handleChange = (e) => {
        let {name, value} = e.target;
        this.setState({
            [name]: value
        })
    }
    handleAddTask = () => {
        let {taskName} = this.state;
        let newTask = {
            id: Date.now(),
            taskName,
            done: false,
        }
        this.setState({
            taskName: ''
        }, () => {
            this.props.dispatch(addTaskAction(newTask));
        })
    }

    renderTheme = () => {
        return arrTheme.map((theme, index) => {
            return (
                <option key={index} value={theme.id}>{theme.name}</option>
            )
        })
    }
    // componentWillReceiveProps(newProps) {
    //     this.setState({
    //         taskName: newProps.taskEdit.taskName,
    //     })
    // }

    render() {
    return (
        <ThemeProvider theme={this.props.themeToDoList}>
            <div>
                <Container className='w-50'>
                    <Dropdown 
                    onChange={(e) => {
                        let {value} = e.target;
                        this.props.dispatch(changeThemeAction(value))
                    }}>
                        {this.renderTheme()}
                    </Dropdown>
                    <Heading2>To Do List</Heading2>
                    <TextField 
                    value={this.state.taskName}
                    onChange={this.handleChange}
                    label={'Task name'} name='taskName' className='w-50'/>
                    <Button onClick={this.handleAddTask} className='ml-2'><i className="fa-solid fa-plus"></i> Add task</Button>
                    {
                        this.state.disabled ? <Button disabled className='ml-2'><i className="fa-solid fa-upload"></i> Update task</Button> : 
                        <Button
                        onClick={() => {
                            let {taskName} = this.state;
                            this.setState({
                                disabled: true,
                                taskName: '',
                            }, () => {
                                this.props.dispatch(updateTaskAction(taskName))
                            })
                        }}
                        className='ml-2'><i className="fa-solid fa-upload"></i> Update task</Button>
                    }
                    <hr />
                    <Heading2>Task To Do</Heading2>

                    <Table>
                        <Thead>
                            {this.renderTaskToDo()}
                        </Thead>
                    </Table>

                    <Heading2>Task completed</Heading2>
                    <Table>
                        <Thead>
                            {this.renderTaskCompleted()}
                        </Thead>
                    </Table>
                </Container>
            </div>
        </ThemeProvider>
    )
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.taskEdit.id !== this.props.taskEdit.id) {
        this.setState({
            taskName: this.props.taskEdit.taskName
        })
    }
  }
}

let mapStateToProps = (state) => {
    return {
        themeToDoList: state.toDoListReducer.themeToDoList,
        taskList: state.toDoListReducer.taskList,
        taskEdit: state.toDoListReducer.taskEdit
    }
}

export default connect(mapStateToProps)(ToDoList);
