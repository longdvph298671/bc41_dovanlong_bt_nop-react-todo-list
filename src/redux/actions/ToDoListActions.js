import { add_task, change_theme, delete_task, done_task, edit_task, update_task } from "../types/ToDoListTypes";


export const addTaskAction = (newTask) => ({
  type: add_task,
  newTask
})
export const changeThemeAction = (id) => ({
  type: change_theme,
  payload: id
})
export const doneTaskAction = (taskId) => ({
  type: done_task,
  payload: taskId
})
export const deletaTaskAction = (taskId) => ({
  type: delete_task,
  payload: taskId
})
export const editTaskAction = (task) => ({
  type: edit_task,
  payload: task
})
export const updateTaskAction = (taskName) => ({
  type: update_task,
  payload: taskName
})




