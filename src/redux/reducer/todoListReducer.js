import { arrTheme } from "../../TodoList/themes/ThemeManager";
import { ToDoListDarkTheme } from "../../TodoList/themes/ToDoListDarkTheme";
import { add_task, change_theme, delete_task, done_task, edit_task, update_task } from "../types/ToDoListTypes";

let initialValue = {
    themeToDoList: ToDoListDarkTheme,
    taskList: [
        {id: 'task-1', taskName: 'task 1', done: true},
        {id: 'task-2', taskName: 'task 2', done: false},
        {id: 'task-3', taskName: 'task 3', done: true},
        {id: 'task-4', taskName: 'task 4', done: false},
    ],
    taskEdit: {id: 'task-1', taskName: 'task 1', done: false}
}

export const toDoListReducer = (state = initialValue, action) => {
    switch(action.type) {
        case add_task: {
            if(action.newTask.taskName.trim() === '') {
                alert('Task name is required!');
                return {...state}
            }
            let cloneTaskList = [...state.taskList];
            let index = cloneTaskList.findIndex((task)=> {
                return task.taskName === action.newTask.taskName;
            })
            console.log("🚀 ~ file: todoListReducer.js:31 ~ toDoListReducer ~ action.taskName:", action.newTask)
            if(index !== -1) {
                alert('Task name already exists!')
                return {...state}
            }
            else {
                cloneTaskList.push(action.newTask);
                return {...state, taskList: cloneTaskList}
            }
            
        }
        case change_theme: {
            let theme = arrTheme.find(item => item.id == action.payload)
            if(theme) {
                return {...state, themeToDoList: theme.theme}
            }
        }
        case done_task: {
            console.log(action)
            let cloneTaskList = [...state.taskList]
            let index = cloneTaskList.findIndex(item => item.id === action.payload)
            if(index !== -1) {
                cloneTaskList[index].done = true
            }
            return {...state, taskList: cloneTaskList}
        }
        case delete_task: {
            let cloneTaskList = [...state.taskList]
            let newTaskList = cloneTaskList.filter(item => item.id !== action.payload)
            return {...state, taskList: newTaskList}
        }
        case edit_task: {
            return {...state, taskEdit: action.payload}
        }
        case update_task: {
            state.taskEdit = {...state.taskEdit, taskName: action.payload}
            let cloneTaskList = [... state.taskList]
            let index = cloneTaskList.findIndex(item => item.id === state.taskEdit.id)
            if(index !== -1) {
                cloneTaskList[index] = state.taskEdit
            }
            state.taskList = cloneTaskList
            state.taskEdit = {id: '-1', taskName: '', done: false}
            return {...state}
        }
        default:
            return {...state};
    }
}