import { combineReducers } from "redux";
import { toDoListReducer } from "./todoListReducer";



export const rootReducer_ToDoList = combineReducers({
    toDoListReducer: toDoListReducer
})