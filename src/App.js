import logo from './logo.svg';
import './App.css';
import ToDoList from './TodoList/ToDoList';

function App() {
  return (
    <div className="App">
      <ToDoList/>
    </div>
  );
}

export default App;
